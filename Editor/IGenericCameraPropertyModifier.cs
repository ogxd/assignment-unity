﻿using UnityEngine;

public interface IGenericCameraPropertyModifier
{
    bool IsHDRAllowed { get; }
    bool IsCameraValid { get; }
    Color BackgroundColor { get; set; }
}

#if HDRP

public class HDRPCameraPropertyModifier : IGenericCameraPropertyModifier
{
    public Color BackgroundColor {
        get
        {
            return Camera.main.GetComponent<UnityEngine.Rendering.HighDefinition.HDAdditionalCameraData>().backgroundColorHDR;
        }
        set
        {
            Camera.main.GetComponent<UnityEngine.Rendering.HighDefinition.HDAdditionalCameraData>().backgroundColorHDR = value;
        }
    }

    public bool IsCameraValid
    {
        get
        {
            if (!Camera.main)
                return false;

            if (!Camera.main.GetComponent<UnityEngine.Rendering.HighDefinition.HDAdditionalCameraData>())
                return false;

            return true;
        }
    }

    public bool IsHDRAllowed => true;
}

#elif URP

public class URPCameraPropertyModifier : IGenericCameraPropertyModifier
{
    public Color BackgroundColor
    {
        get
        {
            return Camera.main.backgroundColor;
        }
        set
        {
            Camera.main.backgroundColor = value;
        }
    }

    public bool IsCameraValid
    {
        get
        {
            if (!Camera.main)
                return false;

            return true;
        }
    }

    public bool IsHDRAllowed => Camera.main.allowHDR;
}

#else

public class StandardCameraPropertyModifier : IGenericCameraPropertyModifier
{
    public Color BackgroundColor
    {
        get
        {
            return Camera.main.backgroundColor;
        }
        set
        {
            Camera.main.backgroundColor = value;
        }
    }

    public bool IsCameraValid
    {
        get
        {
            if (!Camera.main)
                return false;

            return true;
        }
    }

    public bool IsHDRAllowed => Camera.main.allowHDR;
}

#endif