﻿using UnityEngine;
using UnityEditor;

public class MainCameraBackgroundColorWindow : EditorWindow
{
    [MenuItem("Test/Main Camera Background Color")]
    private static void Open()
    {
        var window = GetWindow<MainCameraBackgroundColorWindow>("Main Camera Background Color");
        window.Show();
    }

    private IGenericCameraPropertyModifier genericCameraPropertyModifier;

    private void OnEnable()
    {
#if HDRP
        genericCameraPropertyModifier = new HDRPCameraPropertyModifier();
#elif URP
        genericCameraPropertyModifier = new URPCameraPropertyModifier();
#else
        if (UnityEngine.Rendering.GraphicsSettings.renderPipelineAsset == null)
        {
            genericCameraPropertyModifier = new StandardCameraPropertyModifier();
        }
#endif
    }

    private void OnGUI()
    {
        if (genericCameraPropertyModifier == null)
        {
            EditorGUILayout.HelpBox("This tool must be implemented manually for custom scriptable render pipelines", MessageType.Warning);
            return;
        }

        if (!genericCameraPropertyModifier.IsCameraValid)
        {
            EditorGUILayout.HelpBox("Please check the camera setup", MessageType.Warning);
            return;
        }

        Color backgroundColor = genericCameraPropertyModifier.BackgroundColor;

        EditorGUI.BeginChangeCheck();

        backgroundColor = EditorGUILayout.ColorField(new GUIContent("Background Color"), backgroundColor, true, true, genericCameraPropertyModifier.IsHDRAllowed);

        if (EditorGUI.EndChangeCheck())
        {
            genericCameraPropertyModifier.BackgroundColor = backgroundColor;
        }
    }
}